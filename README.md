PHP developer task:
Please prepare a simple PHP app for exporting data from the attached XML file to Redis.
The app needs to be done the way we can run it in a cloud environment.
You have the freedom to choose if you will use any framework or you will write it in vanilla PHP.
Please focus on simplicity, proper logic, and tests.
Necessary steps:
- From attached XML file, please export data to Redis,
- key &quot;subdomains&quot; will contain JSON with all subdomains (e.g.
[&quot;http://secureline.tools.avast.com&quot;, &quot;http://gf.tools.avast.com&quot;]),
- keys &quot;cookie:%NAME%:%HOST%&quot; will contain values of cookie elements (e.g. key
&quot;cookie:dlp-avast:amazon&quot; will contain string &quot;mmm_amz_dlp_777_ppc_m&quot;),
- use docker-compose for setting up cloud environment (PHP and Redis needs to have their
own containers),
- please use PHPUnit for tests.
- to run the app please use this command:
export.sh /path/to/xml
- if &quot;-v&quot; argument is present in command it should print all keys saved to Redis
export.sh -v /path/to/xml

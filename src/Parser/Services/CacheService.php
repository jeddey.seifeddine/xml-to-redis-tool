<?php

namespace Parser\Services;

use Parser\Infrastructure\Dto\DataModelCollection;
use Parser\Infrastructure\Parser\FileParser;

/**
 * Class CacheService
 * @package Parser\Services
 */
class CacheService
{
    /**
     * @var $fileParser FileParser
     */
    protected $fileParser ;

    /**
     * CacheService constructor.
     * @param FileParser $fileParser
     */
    public function __construct(FileParser $fileParser)
    {
        $this->fileParser = $fileParser;
    }

    /**
     * @param $file
     * @param $extension
     * @return DataModelCollection
     */
    public function process($file,$extension): DataModelCollection
    {
        return $this->fileParser->process($file,$extension);
    }
}
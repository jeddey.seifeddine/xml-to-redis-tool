<?php


namespace Parser\Infrastructure\Validator;


use DOMDocument;
use Parser\Domain\Validator\FileValidatorInterface;
class XmlValidator implements FileValidatorInterface
{

    /**
     * @param string $file
     * @param array $config
     * @return bool
     * @throws \Exception
     */
    public function isValidContent(string $file)
    {
        $config = [ "version" => '1.0', "encoding" => 'utf-8'];
        $xmlContent = file_get_contents($file);

        if (trim($xmlContent) == '') {
            return false;
        }

        libxml_use_internal_errors(true);

        $doc = new DOMDocument($config['version'], $config['encoding']);
        $doc->loadXML($xmlContent);

        $errors = libxml_get_errors();
        libxml_clear_errors();

        if (!empty($errors)) {
            throw new \Exception(sprintf('file %s is not valid xml.',$file));
        }
        return true;
    }
}
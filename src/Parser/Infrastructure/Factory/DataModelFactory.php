<?php

namespace Parser\Infrastructure\Factory;

use Parser\Infrastructure\Dto\DataModel;

/**
 * Class DataModelFactory
 * @package Parser\Infrastructure\Factory
 */
class DataModelFactory
{
    /**
     * @param $key
     * @param $value
     * @return DataModel
     */
        public static function create( string $key,  $value): DataModel
        {
            return new DataModel($key,$value);
        }
}
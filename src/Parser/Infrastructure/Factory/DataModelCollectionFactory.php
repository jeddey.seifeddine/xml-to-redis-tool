<?php

namespace Parser\Infrastructure\Factory;

use Parser\Infrastructure\Dto\DataModelCollection;

/**
 * Class DataModelCollectionFactory
 * @package Parser\Infrastructure\Factory
 */
class DataModelCollectionFactory
{
    /**
     * @return DataModelCollection
     */
    public static function create(): DataModelCollection
    {
        return new DataModelCollection();
    }
}
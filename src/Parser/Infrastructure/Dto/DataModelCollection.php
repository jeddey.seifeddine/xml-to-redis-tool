<?php

namespace Parser\Infrastructure\Dto;

/**
 * Class DataModelCollection
 * @package Parser\Infrastructure\Dto
 */
class DataModelCollection
{
    /**
     * @var $collection array
     */
    protected $collection = [];

    /**
     * @var $log array
     */
    protected $log = [];

    /**
     * @param DataModel $data
     */
    public function push(DataModel $data): void
    {
        $this->collection[] = $data;
    }


    /**
     * @return array
     */
    public function getCollection(): array
    {
        return $this->collection;
    }

    /**
     * @param DataModel $dataModel
     */
    public function addLog(DataModel $dataModel): void
    {
        $this->log[] = $dataModel->getKey();
    }

    /**
     * @return array
     */
    public function getLog(): array
    {
        return $this->log;
    }
}
<?php

namespace Parser\Infrastructure\Dto;

/**
 * Class DataModel
 * @package Parser\Infrastructure\Dto
 */
class DataModel
{
    /**
     * @var $key String
     */
    protected $key;

    /**
     * @var $value mixed
     */
    protected $value;

    /**
     * DataModel constructor.
     * @param string $key
     * @param $value
     */
    public function __construct(string $key ,$value)
    {
        $this->setKey($key);
        $this->setValue($value);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param ?string $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key): void
    {
        $this->key = $key;
    }

}
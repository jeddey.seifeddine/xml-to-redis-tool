<?php

namespace Parser\Infrastructure\Redis;

use Parser\Infrastructure\Dto\DataModel;
use Parser\Infrastructure\Dto\DataModelCollection;
use Redis;

/**
 * Class RedisClient
 * @package Parser\Infrastructure\Redis
 */
class RedisClient
{
    /**
     * @var $redis Redis
     */
    protected $redis ;

    public function __construct(Redis $redis,$host,$port)
    {
        try {
            $redis->connect($host, $port);
        } catch(\Exception $e) {
            throw new \RuntimeException("Redis failed: " . $e->getMessage(), 0, $e);
        }
        $this->redis = $redis;
        $this->initConfig();
    }

    /**
     *
     */
    protected function initConfig(): void
    {
        $this->redis->multi(Redis::PIPELINE);
    }

    /**
     * @param DataModelCollection $dataModelCollection
     * @return DataModelCollection
     */
    public function cache( DataModelCollection $dataModelCollection): DataModelCollection
    {
        $dataModelCollectionData = $dataModelCollection->getCollection();
        /**
         * @var $dataModel DataModel
         */
        foreach ($dataModelCollectionData as $index => $dataModel) {
            $dataModelCollection = $this->set($dataModel, $dataModelCollection);
        }
        $this->execute();
        return $dataModelCollection ;
    }

    /**
     * @param DataModel $dataModel
     * @param DataModelCollection $dataModelCollection
     * @return DataModelCollection
     */
    public function set(DataModel $dataModel, DataModelCollection $dataModelCollection): DataModelCollection
    {
        $this->redis->set($dataModel->getKey(), is_array($dataModel->getValue()) ?
            json_encode($dataModel->getValue()) : $dataModel->getValue());

        $dataModelCollection->addLog($dataModel);
        return $dataModelCollection;
    }

    /**
     *
     */
    public function execute(): void
    {
        $this->redis->exec();
    }

    /**
     * @param DataModel $dataModel
     * @return string
     */
    public function get(DataModel $dataModel): string
    {
        return $this->redis->get($dataModel->getKey());
    }
}
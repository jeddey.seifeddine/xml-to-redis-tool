<?php


namespace Parser\Infrastructure\Parser;


use Parser\Domain\Parser\ParserStrategyInterface;
use Parser\Infrastructure\Dto\DataModelCollection;
use Parser\Infrastructure\Redis\RedisClient;

class FileParser
{
    /**
     * @var array
     */
    protected $parserHandlerEngines ;

    /**
     * @var $redisClient RedisClient
     */
    protected $redisClient ;

    /**
     * @var $dataCollection DataModelCollection
     */
    protected $dataCollection;

    /**
     * FileParser constructor.
     * @param array $parserHandlerEngines
     * @param RedisClient $redisClient
     * @param DataModelCollection $dataCollection
     */
    public function __construct(array $parserHandlerEngines,RedisClient $redisClient, DataModelCollection $dataCollection)
    {
        $this->parserHandlerEngines = $parserHandlerEngines;
        $this->redisClient = $redisClient ;
        $this->dataCollection = $dataCollection;
    }

    /**
     * @param $file
     * @param $extension
     * @return DataModelCollection
     */
    public function process($file,$extension): DataModelCollection
    {
        /**
         * @var $parserEngineHandler ParserStrategyInterface
         */
        $parserEngineHandler = $this->parserHandlerEngines[$extension];
        $dataModelCollection = $parserEngineHandler->parseFile($file,$this->dataCollection);
        return $this->redisClient->cache($dataModelCollection);
    }

}
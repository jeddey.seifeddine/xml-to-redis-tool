<?php

namespace Parser\Infrastructure\Parser;

use Parser\Domain\Parser\ParserStrategyInterface;
use Parser\Infrastructure\Dto\DataModelCollection;
use Parser\Infrastructure\Factory\DataModelFactory;

/**
 * Class XmlParserStrategy
 * @package Parser\Infrastructure\Parser
 */
class XmlParserStrategy implements ParserStrategyInterface
{
    /**
     *
     */
    CONST ROOT_ELEMENT = 'config';

    /**
     * @param $file
     * @param DataModelCollection $collection
     * @return DataModelCollection
     */
    public function parseFile($file, DataModelCollection $collection): DataModelCollection
    {
        $xmlContent = $this->XMLtoArray(file_get_contents($file));
        $rootXmlContent = $xmlContent[self::ROOT_ELEMENT];

        foreach ($rootXmlContent as  $node) {
            if(!empty($node) ) {
                foreach ($node as $key => $value) {
                    switch ($key) {
                        case "subdomain":
                            $collection->push(DataModelFactory::create('subdomain',$value));
                            break;
                        case "cookie":
                            foreach ($value as $index => $ck) {
                                $name = $ck['@attributes']['name'];
                                $host = $ck['@attributes']['host'];
                                $collection->push(DataModelFactory::create('cookie:'.$name.':'.$host,$ck['_value']));
                            }
                            break;
                    }

                }
            }
        }
        return $collection;
    }

    /**
     * @param $xml
     * @return array
     */
    public function XMLtoArray(string $xml)
    {
        $dom = new \DOMDocument('1.0', 'UTF-8');
        $dom->preserveWhiteSpace = false;
        $dom->loadXml($xml);
        return $this->DOMtoArray($dom);
    }

    /**
     * @param $root
     * @return array
     */
    public function DOMtoArray($root)
    {
        $result = array();
        if ($root->hasAttributes()) {
            $attrs = $root->attributes;
            foreach ($attrs as $attr) {
                $result['@attributes'][$attr->name] = $attr->value;
            }
        }
        if ($root->hasChildNodes()) {
            $children = $root->childNodes;
            if ($children->length == 1) {
                $child = $children->item(0);
                if (in_array($child->nodeType,[XML_TEXT_NODE,XML_CDATA_SECTION_NODE])) {
                    $result['_value'] = $child->nodeValue;
                    return count($result) == 1
                        ? $result['_value']
                        : $result;
                }

            }
            $groups = array();
            foreach ($children as $child) {
                if (!isset($result[$child->nodeName])) {
                    $result[$child->nodeName] = $this->DOMtoArray($child);
                } else {
                    if (!isset($groups[$child->nodeName])) {
                        $result[$child->nodeName] = array($result[$child->nodeName]);
                        $groups[$child->nodeName] = 1;
                    }
                    $result[$child->nodeName][] = $this->DOMtoArray($child);
                }
            }
        }
        return $result;
    }
}
<?php

namespace Parser\Domain\Parser;

use Parser\Infrastructure\Dto\DataModelCollection;

/**
 * Interface ParserStrategyInterface
 * @package Parser\Domain\Parser
 */
interface ParserStrategyInterface
{
    /**
     * @param $file
     * @param DataModelCollection $collection
     * @return DataModelCollection
     */
    public function parseFile($file,DataModelCollection $collection): DataModelCollection;
}
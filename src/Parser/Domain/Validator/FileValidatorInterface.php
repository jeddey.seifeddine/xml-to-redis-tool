<?php

namespace Parser\Domain\Validator;

/**
 * Interface FileValidatorInterface
 * @package Parser\Domain\Validator
 */
interface FileValidatorInterface
{
    /**
     * @param string $file
     * @param array $config
     * @return bool
     */
    public function isValidContent(string $file);
}
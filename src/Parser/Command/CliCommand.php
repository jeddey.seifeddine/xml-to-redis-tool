<?php

namespace Parser\Command;

use Parser\Domain\Validator\FileValidatorInterface;
use Parser\Infrastructure\Dto\DataModelCollection;
use Parser\Services\CacheService;
use Parser\Services\CacheService as CacheServiceAlias;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class CliCommand
 * @package Parser\Command
 */
final class CliCommand extends Command
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * CliCommand constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct($name = null);
    }


    /**
     *
     */
    protected function configure(): void
    {
        $this->setName('file');
        $this->setDescription('Provide an xml file path to parse.');
        $this->addArgument('file', InputArgument::REQUIRED, 'File to be parsed');
        $this->addOption('--logging',null,InputOption::VALUE_OPTIONAL);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): bool
    {
        /**
         * @var $fileSystem Filesystem
         */
        $allowedExtensions = $this->container->getParameter('parser')['allowedExtensions'];
        $file = $input->getArgument('file');
        $logging = $input->getOption('logging');

        try {
            $uploadFile = new UploadedFile($file,$file);
        } catch (FileNotFoundException $e) {
            $output->writeln($e->getMessage());
            return false;
        }

        $extension = strtolower($uploadFile->getExtension());
        if (!in_array($extension,$allowedExtensions)) {
            $output->writeln(sprintf(
                '%s extension is not allowed , please check the file again.', $file
            ));
            return false;
        }

        /**
         * @var $validatorService FileValidatorInterface
         */
        $validator        = "Parser\\Infrastructure\\Validator\\".ucfirst($extension)."Validator";
        $validatorService = $this->container->get($validator);

        try {
            $validatorService->isValidContent($file);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return false;
        }

        /**
         * @var $CacheService CacheService
         */
        $CacheService = $this->container->get(CacheServiceAlias::class);
        $output->writeln(sprintf("Start indexing document: %s to Redis", $file));
        /**
         * @var $dataModelCollection DataModelCollection
         */
        $dataModelCollection = $CacheService->process($file,$extension);

        if ( $logging == 'true') {
            $output->writeln("Logging Mode is enabled.");
            $output->writeln("Inserted Keys : ");
            foreach ($dataModelCollection->getLog() as $log) {
                $output->writeln($log);
            }
        }
        $output->writeln(sprintf("Document %s has been  indexed", $file));
        return true;
    }
}
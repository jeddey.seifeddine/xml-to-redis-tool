<?php

use Parser\Infrastructure\Dto\DataModelCollection;
use Parser\Infrastructure\Parser\FileParser;
use Parser\Infrastructure\Parser\XmlParserStrategy;
use Parser\Infrastructure\Redis\RedisClient;
use Parser\Infrastructure\Validator\XmlValidator;
use Parser\Services\CacheService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Filesystem\Filesystem;


/** @var ContainerBuilder $container */

$container->register(redis::class,Redis::class);
$container->register(RedisClient::class,RedisClient::class)
    ->addArgument(new Reference(redis::class))
    ->addArgument('%redisHost%')
    ->addArgument('%redisPort%')
;

$container->register(Filesystem::class ,Filesystem::class);
$container->register(XmlValidator::class ,XmlValidator::class);

$container->register(DataModelCollection::class ,DataModelCollection::class);

$container->register(XmlParserStrategy::class ,XmlParserStrategy::class);

$this->container->register(FileParser::class, FileParser::class)
    ->addArgument(['xml' => new Reference(XmlParserStrategy::class)])
->addArgument(new Reference(RedisClient::class))
->addArgument(new Reference(DataModelCollection::class));



$container->register(CacheService::class,CacheService::class)
    ->addArgument(new Reference(FileParser::class));






#!/bin/bash


if test "$#" -gt 2; then
  echo "Xml To Redis Parser Tool ."
  echo "Illegal number of parameters"
  echo "Usage: sh export.sh -v <Verbose|Optional> path/to/file.xml <File>"
  exit
fi

file="$1"
logging="false"

if test "$#" -eq 2; then
  file="$2"
  logging="$1"
fi

if [[ ${logging} == "-v" ]]
then
  logging="true"
fi
docker-compose exec httpd bash -c "php /var/www/bin/console file ${file} --logging=${logging}"

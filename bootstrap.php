<?php
define('BASE_DIR', __DIR__);
define('RESOURCES_DIR', BASE_DIR.'/resources');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
ini_set('register_argc_argv',1);
require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * @return ContainerBuilder
 * @throws Exception
 */
function loadContainer(): ContainerBuilder
{
    $container = new ContainerBuilder();

    $fileLoader = new PhpFileLoader($container,new FileLocator());
    $fileLoader->load(RESOURCES_DIR.'/wiring/common.php');

    $fileLoader = new YamlFileLoader($container,new FileLocator());
    $fileLoader->load(RESOURCES_DIR.'/config/config.yaml');
    $container->compile(TRUE);

    return $container;
}

// Create the Application
$application = new Symfony\Component\Console\Application;

try {
    $container = loadContainer();
} catch (Throwable $t) {
    error_log(sprintf('%s in %d at line %s', $t->getMessage(), $t->getFile(), $t->getLine()));
    error_log($t->getTraceAsString());
    exit(E_ERROR);
}
// Register all Commands
$application->add(new Parser\Command\CliCommand($container));

<?php

namespace Parser\Command;

use PHPUnit\Framework\TestCase;
use ReflectionClass;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class CliCommandTest
 * @package Parser\Command
 */
class CliCommandTest extends TestCase
{
    /**
     * @var $container ContainerBuilder
     */
    protected $container;

    /**
     * @throws \Exception
     */
    public function setUp(): void
    {
        $container = new ContainerBuilder();
        $fileLoader = new PhpFileLoader($container,new FileLocator());
        $fileLoader->load(RESOURCES_DIR.'/wiring/common.php');
        $fileLoader = new YamlFileLoader($container,new FileLocator());
        $fileLoader->load(RESOURCES_DIR.'/config/config.yaml');
        $container->compile(TRUE);
        $this->container = $container;

        parent::setUp();
    }

    public function testExecuteCli(): void
    {

        $definition = new InputDefinition([
            new InputArgument('file', InputArgument::REQUIRED,'File to be parsed'),
         new InputOption('logging', '-v')]);
        $cliCommand = new CliCommand($this->container);
        $input = new ArgvInput(['file', './tests/files/config.xml','--logging'],$definition);
        $output = new ConsoleOutput;
        $reflection = new ReflectionClass(get_class($cliCommand));
        $method = $reflection->getMethod('execute');
        $method->setAccessible(true);
        $result = $method->invoke($cliCommand,  $input,$output);
        $this->assertTrue(true,$result);
    }

    public function testCliCommandNotExist(): void
    {
        $definition = new InputDefinition([
            new InputArgument('file', InputArgument::REQUIRED,'File to be parsed'),
            new InputOption('--logging', '-v')]);
        $cliCommand = new CliCommand($this->container);
        $input = new ArgvInput(['file', './tests/files/config.html','-v'],$definition);
        $output = new ConsoleOutput;
        $reflection = new ReflectionClass(get_class($cliCommand));
        $method = $reflection->getMethod('execute');
        $method->setAccessible(true);
        $result = $method->invoke($cliCommand,  $input,$output);
        $this->assertFalse(false,$result);
    }

    public function testCliCommandExtensionNotAllowed(): void
    {
        $definition = new InputDefinition([
            new InputArgument('file', InputArgument::REQUIRED,'File to be parsed'),
            new InputOption('--logging', '-v')]);
        $cliCommand = new CliCommand($this->container);
        $input = new ArgvInput(['file', './tests/files/config.txt','-v'],$definition);
        $output = new ConsoleOutput;
        $reflection = new ReflectionClass(get_class($cliCommand));
        $method = $reflection->getMethod('execute');
        $method->setAccessible(true);
        $result = $method->invoke($cliCommand,  $input,$output);
        $this->assertFalse(false,$result);
    }

    public function testCliCommandInvalidXml(): void
    {
        $container = new ContainerBuilder();
        $fileLoader = new PhpFileLoader($container,new FileLocator());
        $fileLoader->load(RESOURCES_DIR.'/wiring/common.php');
        $fileLoader = new YamlFileLoader($container,new FileLocator());
        $fileLoader->load(RESOURCES_DIR.'/config/config.yaml');
        $definition = new InputDefinition([
            new InputArgument('file', InputArgument::REQUIRED,'File to be parsed'),
            new InputOption('--logging', '-v')]);
        $cliCommand = new CliCommand($container);
        $input = new ArgvInput(['file', './tests/files/invalid.xml','-v'],$definition);
        $output = new ConsoleOutput;
        $reflection = new ReflectionClass(get_class($cliCommand));
        $method = $reflection->getMethod('execute');
        $method->setAccessible(true);
        $result = $method->invoke($cliCommand,  $input,$output);
        $this->assertFalse(false,$result);
    }

    public function tearDown()
    {
        unset($this->container);
    }
}

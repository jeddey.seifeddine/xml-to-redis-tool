<?php

namespace Parser\Infrastructure\Parser;

use Parser\Infrastructure\Dto\DataModelCollection;
use Parser\Infrastructure\Redis\RedisClient;
use PHPUnit\Framework\TestCase;
use Redis;

/**
 * Class FileParserTest
 * @package Parser\Infrastructure\Parser
 */
class FileParserTest extends TestCase
{
    public function testProcess(): void
    {
        $xmlParserStrategy = new XmlParserStrategy;
        $parserHandlerEngines = ['xml' => $xmlParserStrategy ];
        $redisClient = new RedisClient(new Redis(),$host = 'host.docker.internal',$port =6382 );
        $dataModelCollection = new DataModelCollection;
        $fileParser = new FileParser($parserHandlerEngines,$redisClient,$dataModelCollection);
        $result = $fileParser->process('./test/file/config.xml','xml');
        $this->assertInstanceOf(DataModelCollection::class,$result);
    }

    public function testRedisWithWrongParams(): void
    {
        $this->expectException(\RuntimeException::class);
        $xmlParserStrategy = new XmlParserStrategy;
        $parserHandlerEngines = ['xml' => $xmlParserStrategy ];
        $redisClient = new RedisClient(new Redis(),$host = 'invalid.docker.internal',$port =6382 );
        $dataModelCollection = new DataModelCollection;
        $fileParser = new FileParser($parserHandlerEngines,$redisClient,$dataModelCollection);
        $result = $fileParser->process('./test/file/config.xml','xml');
        $this->assertInstanceOf(DataModelCollection::class,$result);
    }
}
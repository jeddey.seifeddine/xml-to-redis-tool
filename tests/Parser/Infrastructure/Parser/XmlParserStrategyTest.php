<?php

namespace Parser\Infrastructure\Parser;

use Parser\Infrastructure\Dto\DataModelCollection;
use Parser\Infrastructure\Factory\DataModelCollectionFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class XmlParserStrategyTest
 * @package Parser\Infrastructure\Parser
 */
class XmlParserStrategyTest extends TestCase
{
    public function testXMLtoArrayWithInvalidXml(): void
    {
        $xmlParserStrategy = new XmlParserStrategy;
        $xmlContent = file_get_contents('./tests/files/config.xml');
        $result = $xmlParserStrategy->XMLtoArray($xmlContent);
        $this->assertTrue(true,empty($result));
    }

    public function testParseFile(): void
    {
        $xmlParserStrategy = new XmlParserStrategy;
        $dataModelCollection = DataModelCollectionFactory::create();
        $result = $xmlParserStrategy->parseFile('./tests/files/config.xml',$dataModelCollection);
        $this->assertInstanceOf(DataModelCollection::class,$result);
    }
}
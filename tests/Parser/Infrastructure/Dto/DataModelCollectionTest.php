<?php

namespace Parser\Infrastructure\Dto;

use PHPUnit\Framework\TestCase;

class DataModelCollectionTest extends TestCase
{
    public function testPushataModel(): void
    {
        $dataModelCollection = new DataModelCollection();
        $key = "master_key";
        $value = "value";
        $dataModel = new DataModel($key,$value);
        $dataModelCollection->push($dataModel);
        $collection = $dataModelCollection->getCollection();
        $this->assertTrue(true,!empty($collection));
    }

    public function testaddAndGetLog(): void
    {
        $dataModelCollection = new DataModelCollection();
        $key = "master_key";
        $value = "value";
        $dataModel = new DataModel($key,$value);
        $dataModelCollection->addLog($dataModel);;
        $this->assertTrue(true,in_array($key,$dataModelCollection->getLog()));
    }


}
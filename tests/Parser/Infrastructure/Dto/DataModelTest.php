<?php


namespace Parser\Infrastructure\Dto;


use Parser\Infrastructure\Dto\DataModel;
use PHPUnit\Framework\TestCase;

class DataModelTest extends TestCase
{
    public function testKeySetsuccessfully(): void
    {
        $key = "master_key";
        $value = "value";
        $dataModel = new DataModel($key,$value);
        $this->assertEquals($key,$dataModel->getKey());
    }

    public function testRetreivCorrectValue(): void
    {
        $key = "master_key";
        $value = "value";
        $dataModel = new DataModel($key,$value);
        $this->assertEquals($value,$dataModel->getValue());
    }
}
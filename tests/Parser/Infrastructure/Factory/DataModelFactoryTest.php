<?php

namespace Parser\Infrastructure\Factory;

use Parser\Infrastructure\Dto\DataModel;
use PHPUnit\Framework\TestCase;

/**
 * Class DataModelFactoryTest
 * @package Parser\Infrastructure\Factory
 */
class DataModelFactoryTest extends TestCase
{
    public function testDataModelFactory(): void
    {
        $key = "master_key";
        $value = "value";
        $dataModel =  DataModelFactory::create($key,$value);
        $this->assertInstanceOf(DataModel::class,$dataModel);
    }

}
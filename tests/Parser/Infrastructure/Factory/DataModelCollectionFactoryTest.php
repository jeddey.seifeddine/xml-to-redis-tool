<?php

namespace Parser\Infrastructure\Factory;

use Parser\Infrastructure\Dto\DataModelCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class TestDataModelCollectionFactory
 * @package Parser\Infrastructure\Factory
 */
class DataModelCollectionFactoryTest extends TestCase
{

    public function testCreateDataModelcollectionInstance(): void
    {
        $dataModelCollection = DataModelCollectionFactory::create();
        $this->assertTrue(true,$dataModelCollection instanceof DataModelCollection);
    }

}
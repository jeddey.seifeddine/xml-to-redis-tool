<?php

namespace Parser\Infrastructure\Validator;

use PHPUnit\Framework\TestCase;

/**
 * Class XmlValidatorTest
 * @package Parser\Infrastructure\Validator
 */
class XmlValidatorTest extends TestCase
{
    public function testXmlValidator(): void
    {
        $XmlValidator = new XmlValidator;
        $isValid = $XmlValidator->isValidContent('./tests/files/config.xml');
        $this->assertTrue(true,$isValid);
    }

    public function testEmptyXml(): void
    {
        $XmlValidator = new XmlValidator;
        $result = $XmlValidator->isValidContent('./tests/files/empty.xml');
        $this->assertFalse(false,$result);
    }

    public function testInvalidXml(): void
    {
        $this->expectException(\Exception::class);
        $XmlValidator = new XmlValidator;
        $XmlValidator->isValidContent('./tests/files/invalid.xml');
    }
}
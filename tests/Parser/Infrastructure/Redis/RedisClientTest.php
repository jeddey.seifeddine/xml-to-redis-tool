<?php

namespace Parser\Infrastructure\Redis;

use Parser\Infrastructure\Dto\DataModel;
use Parser\Infrastructure\Factory\DataModelCollectionFactory;
use PHPUnit\Framework\TestCase;
use Redis;

/**
 * Class RedisClientTest
 * @package Parser\Infrastructure\Redis
 */
class RedisClientTest extends TestCase
{
    public function testSet(): void
    {
        $key = "master_key";
        $value = "value";
        $dataModel = new DataModel($key,$value);
        $redisClient = new RedisClient(new Redis(),$host = 'host.docker.internal',$port =6382 );
        $dataModelCollection = DataModelCollectionFactory::create();
        $redisClient->set( $dataModel,  $dataModelCollection);
        $redisClient->execute();;
        $this->assertEquals($value,$redisClient->get($dataModel));


    }
}
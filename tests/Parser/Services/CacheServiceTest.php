<?php

namespace Parser\Services;

use Parser\Infrastructure\Dto\DataModelCollection;
use Parser\Infrastructure\Parser\FileParser;
use Parser\Infrastructure\Parser\XmlParserStrategy;
use Parser\Infrastructure\Redis\RedisClient;
use Parser\Services\CacheService as CachingService;
use PHPUnit\Framework\TestCase;
use Redis;

/**
 * Class CacheServiceTest
 * @package Parser\Services
 */
class CacheServiceTest extends TestCase
{
    public function testprocess(): void
    {
        $xmlParserStrategy = new XmlParserStrategy;
        $parserHandlerEngines = ['xml' => $xmlParserStrategy ];
        $redisClient = new RedisClient(new Redis(),$host = 'host.docker.internal',$port =6382 );
        $dataModelCollection = new DataModelCollection;
        $fileParser = new FileParser($parserHandlerEngines,$redisClient,$dataModelCollection);
        $cacheService = new CachingService($fileParser);
        $result = $cacheService->process('./test/file/config.xml','xml');
        $this->assertInstanceOf(DataModelCollection::class,$result);
    }
}